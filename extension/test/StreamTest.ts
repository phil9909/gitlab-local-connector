import 'mocha';
import {Stream, StreamSource} from '../src/Stream';
import { expect, AssertionError } from 'chai';

describe('StreamSource', () => {
  it('call forEach then call next', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    stream.forEach(result.push.bind(result));

    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal([1,2,40]);
  });

  it('call next then call forEach', () => {
    const stream = new StreamSource<number>((next) => {
      next(1);
      next(2);
      next(40);
    });

    let result: number[] = [];
    stream.forEach(result.push.bind(result));

    expect(result).to.deep.equal([1,2,40]);
  });

  it('call next then call forEach then call next', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
      next(1);
      next(2);
      next(40);
    });

    let result: number[] = [];
    stream.forEach(result.push.bind(result));

    next(101);
    next(102);
    next(140);

    expect(result).to.deep.equal([1,2,40,101,102,140]);
  });

  it('call end', (done) => {
    const stream = new StreamSource<number>((_, end) => {
      end(undefined);
    });

    let result: number[] = [];
    let ended = false;
    stream
      .forEach(result.push.bind(result))
      .then(_ => ended = true);

    expect(result.length).to.equal(0);
    setTimeout(() => {
      expect(ended).to.equal(true);
      done();
    }, 0);
  });

  it('call end (non void)', (done) => {
    const stream = new StreamSource<number,string>((_, end) => {
      end("hello world");
    });

    let result: number[] = [];
    let ended = "";
    stream
      .forEach(result.push.bind(result))
      .then(e => ended = e);

    expect(result.length).to.equal(0);
    setTimeout(() => {
      expect(ended).to.equal("hello world");
      done();
    }, 0);
  });

  it('call forEach twice', () => {
    const stream = new StreamSource<number>((next) => {
      next(1);
      next(2);
      next(40);
    });

    let result: number[] = [];
    stream.forEach(result.push.bind(result));

    expect(stream.forEach.bind(stream, console.error.bind(console))).to.throw("Stream already used");
    expect(result).to.deep.equal([1,2,40]);
  });

  it('call forEach then call error', () => {
    
    let error: ((e: Error) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((_, x, e) => {
      error = e;
    });

    let result: number[] = [];
    let errors: Error[] = [];
    stream.forEach(result.push.bind(result), errors.push.bind(errors));

    error(new Error("expected error"));

    expect(result.length).to.equal(0);
    expect(errors.length).to.equal(1);
    expect(errors[0].message).to.equal("expected error");
  });

  it('call error then call forEach', () => {
    const stream = new StreamSource<number>((_, x, error) => {
      error(new Error("expected error"));
    });

    let result: number[] = [];
    let errors: Error[] = [];
    stream.forEach(result.push.bind(result), errors.push.bind(errors));

    expect(result.length).to.equal(0);
    expect(errors.length).to.equal(1);
    expect(errors[0].message).to.equal("expected error");
  });

  it('next after end', () => {
    const stream = new StreamSource<number>((next, end, error) => {
      next(1);
      end(undefined);
      expect(next.bind(2)).to.throw("can't call next on ended stream");
    });

    let result: number[] = [];
    stream.forEach(result.push.bind(result));

    expect(result).to.deep.equal([1]);
  });

  it('error after end', () => {
    const stream = new StreamSource<number>((next, end, error) => {
      end(undefined);
      expect(error.bind(new Error(""))).to.throw("can't call error on ended stream");
    });

    let result: number[] = [];
    let errors: Error[] = [];
    stream.forEach(result.push.bind(result), errors.push.bind(errors));

    expect(result.length).to.equal(0);
    expect(errors.length).to.equal(0);
  });

  it('recover from errror and call forEach afterwards, check order', () => {
    const stream = new StreamSource<number>((next, end, error) => {
      next(4);
      next(5);
      error(new Error("expected error"));
      next(40);
      next(50);
      end(undefined);
    });

    let result: (number|Error)[] = [];
    stream.forEach(result.push.bind(result), result.push.bind(result));

    expect(result.length).to.equal(5);
    expect(result[0]).to.equal(4);
    expect(result[1]).to.equal(5);
    expect(typeof result[2]).to.equal("object");
    expect(result[3]).to.equal(40);
    expect(result[4]).to.equal(50);
  });

  it('Stream of type error', () => {
    const stream = new StreamSource<Error>((next, end, error) => {
      next(new Error("4"));
      next(new Error("5"));
      error(new Error("expected error"));
      next(new Error("40"));
      next(new Error("50"));
      end(undefined);
    });

    let result: Error[] = [];
    let errors: Error[] = [];
    stream.forEach(result.push.bind(result), (error) => {
      result.push(error);
      errors.push(error);
    });

    expect(result.map(e => e.message)).to.deep.equal(["4","5","expected error","40","50"]);
    expect(errors.length).to.equal(1);
    expect(errors[0].message).to.equal("expected error");
  });
});

describe('MappedStream', () => {
  it('map to same datatype', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    stream
      .map(x => x*2)
      .forEach(result.push.bind(result));

    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal([2,4,80]);
  });

  it('map to string', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    stream
      .map(x => `x: ${x}`)
      .forEach(result.push.bind(result));

    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal(["x: 1","x: 2","x: 40"]);
  });

  it('map twice', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    stream
      .map(x => x*2)
      .map(x => `x: ${x}`)
      .forEach(result.push.bind(result));

    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal(["x: 2","x: 4","x: 80"]);
  });

  it('call end', (done) => {
    const stream = new StreamSource<number>((_, end) => {
      end(undefined);
    });

    let result: number[] = [];
    let ended = false;
    stream
      .map(x => x*2)
      .map(x => `x: ${x}`)
      .forEach(result.push.bind(result))
      .then(_ => ended = true);

    expect(result.length).to.equal(0);
    setTimeout(() => {
      expect(ended).to.equal(true);
      done();
    }, 0);
  });

  it('call end (non void)', (done) => {
    const stream = new StreamSource<number,string>((_, end) => {
      end("hello world");
    });

    let result: number[] = [];
    let ended = "";
    stream
      .map(x => x*2)
      .map(x => `x: ${x}`)
      .forEach(result.push.bind(result))
      .then(e => ended = e);

    expect(result.length).to.equal(0);
    setTimeout(() => {
      expect(ended).to.equal("hello world");
      done();
    }, 0);
  });

  it('call map on used stream (map)', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    let mappedStream = stream
      .map(x => x*2);

    expect(stream.forEach.bind(stream, console.error.bind(console))).to.throw("Stream already used"); 
    expect(stream.map.bind(stream, console.error.bind(console))).to.throw("Stream already used");
    
    mappedStream.forEach(result.push.bind(result));

    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal([2,4,80]);
  });

  it('call map on used stream (forEach)', () => {
    
    let next: ((next: number) => void) = () => {
      throw new AssertionError("This function should not be called. StreamSource did not call the callback.");
    };

    const stream = new StreamSource<number>((n) => {
      next = n;
    });

    let result: number[] = [];
    let mappedStream = stream
      .map(x => x*2);
    mappedStream.forEach(result.push.bind(result));

    expect(mappedStream.forEach.bind(stream, console.error.bind(console))).to.throw("Stream already used"); 
    expect(mappedStream.map.bind(stream, console.error.bind(console))).to.throw("Stream already used");
    
    next(1);
    next(2);
    next(40);

    expect(result).to.deep.equal([2,4,80]);
  });

  it('error propagation', () => {
    const stream = new StreamSource<number>((next, end, error) => {
      next(4);
      next(5);
      error(new Error("expected error"));
      next(40);
      next(50);
      end(undefined);
    });

    let result: (number|Error)[] = [];
    stream
      .map(x => x*2)
      .forEach(result.push.bind(result), result.push.bind(result));

    expect(result.length).to.equal(5);
    expect(result[0]).to.equal(8);
    expect(result[1]).to.equal(10);
    expect(typeof result[2]).to.equal("object");
    expect(result[3]).to.equal(80);
    expect(result[4]).to.equal(100);
  });

  it('error in mapping', () => {
    const stream = new StreamSource<number>((next, end, error) => {
      next(1);
      next(2);
      next(0)
      next(4);
      next(5);
      end(undefined);
    });

    let result: (number|Error)[] = [];
    stream
      .map(x => {
        if( x === 0) {
          throw new Error("Can't devide by 0");
        }
        return 10/x;
      })
      .forEach(result.push.bind(result), result.push.bind(result));

    expect(result.length).to.equal(5);
    expect(result[0]).to.equal(10);
    expect(result[1]).to.equal(5);
    expect(typeof result[2]).to.equal("object");
    expect(result[3]).to.equal(2.5);
    expect(result[4]).to.equal(2);
  });
});