import { endianness } from "os";

export interface Stream<T,E=void> {
    forEach( 
        next: (next: T) => any,
        error? : (error: Error) => void
    ): Promise<E>

    map<M>(
        next: (next: T) => M
    ): Stream<M,E>
}

abstract class AbstractStream<T,E=void> implements Stream<T,E> {

    // We can't use an array of type (T|Error)[] here, because T could be Error (or a subtype of Error)
    private buffer: ([T, undefined]|[undefined,Error])[] = [];
    private errorBufferingHandler(e:Error): void {
        this.buffer.push([undefined,e]);
    }
    private emitBufferingHandler(e:T): void {
        this.buffer.push([e,undefined]);
    }
    private endedFlag = false;
    emit: (e:T) => void
    error: (e:Error) => void
    
    end: (end: E) => void = _ => {throw new Error("Your reached unreachable code")};
    ended: Promise<E> = new Promise((resolve, _) => {
        this.end = (end: E) => {
            this.endedFlag = true;
            resolve(end);
        };
    });

    constructor() {
        this.emit = this.emitBufferingHandler;
        this.error = this.errorBufferingHandler;
    }

    hasEnded(): boolean {
        return this.endedFlag;
    }

    forEach(next: (next: T) => void, error?: (error: Error) => void): Promise<E> {
        if(this.emit != this.emitBufferingHandler) {
            throw new Error("Stream already used");
        }
        if(error == undefined) {
            error = error => {
                console.warn("Unhandled error in stream", error);
            };
        }

        let element: [T, undefined] | [undefined, Error] | undefined;
        while((element = this.buffer.shift()) != undefined) {
            if(element[1] != undefined) {
                error(element[1]);
            }
            else {
                next(<T>(element[0]));
            }
        }
        this.emit = next;
        this.error = error;
        
        return this.ended;
    }
    
    map<M>(mapping: (next: T) => M): Stream<M,E> {
        return new MappedStream(this, mapping);
    }
}
class MappedStream<M,T,E> extends AbstractStream<M,E> {
    constructor(source: Stream<T,E>, mapping: (next: T) => M) {
        super();
        source.forEach( e => {
            try {
                this.emit(mapping(e));
            } catch (e) {
                this.error(e);
            }
        }, e => {
            this.error(e);
        }).then(this.end);
    }
}

export class StreamSource<T,E=void> extends AbstractStream<T,E> {

    private nextCallback(next: T) {
        if(this.hasEnded()){throw new Error("can't call next on ended stream")};
        this.emit(next);
    }
    private endCallback(end: E) {
        this.end(end);
    }
    private errorCallback(error: Error) {
        if(this.hasEnded()){throw new Error("can't call error on ended stream")};
        this.error(error);
    }

    /**
     * Similar to Promises this constructor expects a function reference which itself gets passed functions.
     * Instead of a resolve and reject function the StreamSource class passes a next, end and error function
     */
    constructor(source: (
            next: (next: T) => void,
            end: (end: E) => void,
            error: (error: Error) => void
        ) => void ) {
        super();
        source(this.nextCallback.bind(this), this.endCallback.bind(this), this.errorCallback.bind(this));
    }

}