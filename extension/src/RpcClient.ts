import { HelloPackage, PackageBasis, PackageType, PROTOCOL_VERSION } from './interfaces';
import { Stream, StreamSource } from './Stream';
import { uuid } from './util';

/**
 *
 */

export interface RpcClientOptions {
    endpoint: string;
    retryTimeout?: number;
    callTimeout: number;
}

export interface RpcClientCallOptions {
    timeout?: number;
}

export class RpcClient {

    private ws?: WebSocket;

    private options: RpcClientOptions;

    //tslint:disable-next-line:no-any
    private responseHandler = new Map<string, (err: Error | null, data: any, kind: string) => void>();
    //tslint:disable-next-line:no-any
    private requestHandler = new Map<PackageType, (data: any) => void>();

    constructor(options?: Partial<RpcClientOptions>) {
        this.options = {
            ...{
                endpoint: 'ws://localhost:7365/api/',
                callTimeout: 10000
            },
            ...options
        };
    }

    public connect(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.connectInternal((err) => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });
    }


    public send<T>(kind: PackageType, data: T): void {
        this.sendInternal({
            kind,
            data,
            uuid: uuid()
        });
    }

    public async call<T, T2>(kind: PackageType, data: T, options?: RpcClientCallOptions): Promise<T2> {
        const callUuid = uuid();

        return new Promise<T2>((resolve, reject) => {
            this.responseHandler.set(callUuid, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve(res);
            });
            let timeout = options && options.timeout !== undefined ? options.timeout : this.options.callTimeout;
            if (timeout > 0) {
                setTimeout(
                    () => {
                        reject(new Error('Request Timeout'));
                    },
                    timeout
                );
            }

            this.sendInternal({
                kind,
                data,
                uuid: callUuid
            });
        }).finally(() => {
            this.responseHandler.delete(callUuid);
        });
    }

    private endStreamRegex = /^StreamEnd<(.*)>$/;
    public stream<IN, OUT, END>(kind: PackageType, data: IN, options?: RpcClientCallOptions): Stream<OUT, END | null> {
        const callUuid = uuid();

        return new StreamSource<OUT, END | null>((next, end, error) => {
            this.responseHandler.set(callUuid, (err, res, kind) => {
                if (err) {
                    error(err);
                }
                else if(this.endStreamRegex.test(kind)) {
                    end(res);
                    this.responseHandler.delete(callUuid);
                }
                else {
                    next(res);
                }
            });
            let timeout = options && options.timeout !== undefined ? options.timeout : this.options.callTimeout;
            if (timeout > 0) {
                setTimeout(
                    () => {
                        console.log(`stream timeout after ${timeout}`);
                        error(new Error('Request Timeout'));
                        end(null);
                        this.responseHandler.delete(callUuid);
                    },
                    timeout
                );
            }

            this.sendInternal({
                kind,
                data,
                uuid: callUuid
            });
        })
    }

    public on<T>(kind: PackageType, cb: (data: T) => void): void {
        this.requestHandler.set(kind, cb);
    }

    private connectInternal(callback: (err: Error | null) => void) {
        this.ws = new WebSocket(this.options.endpoint);
        this.ws.addEventListener('message', this.onMessage.bind(this));
        this.ws.addEventListener('open', () => {
            console.debug('websocket connected');
            this.send<HelloPackage>(PackageType.Hello, { uuid: "", version: PROTOCOL_VERSION });

            callback(null);
        });
        this.ws.addEventListener('error', (err) => {
            //console.error('websocket error', err);
            this.ws = undefined;
        });
        this.ws.addEventListener('close', () => {
            console.info('websocket closed');
            this.ws = undefined;

            if (this.options.retryTimeout && this.options.retryTimeout > 0) {
                setTimeout(this.connectInternal.bind(this, callback), this.options.retryTimeout);
            } else {
                callback(new Error("Connection failed (or something else)"));
            }
        });
    }

    private onMessage(evt: MessageEvent): void {
        let obj: PackageBasis;
        try {
            obj = JSON.parse(evt.data);
        } catch (err) {
            console.debug('Invalid Package', err.message);

            return;
        }
        if (!obj || !obj.kind || !obj.uuid) {
            console.debug('Invalid Package', evt.data);

            return;
        }
        let error = null;
        if (obj.kind === PackageType.ErrorResponse) {
            if (!obj.data || !obj.data.message) {
                console.warn("Invalid Error Response", obj);
                error = new Error("error response");
            } else {
                error = new Error(obj.data.message);
            }
        }

        const respHandler = this.responseHandler.get(obj.uuid);
        if (respHandler) {
            respHandler(error, obj.data, obj.kind);

            return;
        }

        const reqHandler = this.requestHandler.get(obj.kind);
        if (reqHandler) {
            reqHandler(obj.data);

            return;
        }

        console.debug('Unhandled package', obj);
    }

    private sendInternal(pkg: PackageBasis): void {
        if (!this.ws) {
            throw new Error('Not connected to backend');
        }
        this.ws.send(JSON.stringify(pkg));
    }
}
