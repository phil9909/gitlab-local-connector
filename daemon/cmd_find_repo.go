package main

import (
	"path/filepath"

	"github.com/speedata/gogit"
)

func cmdFindRepo(ctx *connectionContext, pack packageBasis) {
	request := pack.Data.(findRepoRequest)

	repoLocation := filepath.Join(homeDir, "git", request.RepoName)
	_, err := gogit.OpenRepository(repoLocation)
	if err != nil {
		ctx.local = localRepo{
			RemoteRepositiory: request.RepoName,
			Path:              nil}
		sendResponse(ctx.conn, localRepoType, nil, pack)
	} else {
		ctx.local = localRepo{
			RemoteRepositiory: request.RepoName,
			Path:              &repoLocation}
		sendResponse(ctx.conn, localRepoType, ctx.local, pack)
	}
}
