package main

import (
	"encoding/json"
	"fmt"

	"github.com/mitchellh/mapstructure"
)

type packageType string

const (
	// requests
	helloType           packageType = "Hello"
	findRepoRequestType packageType = "FindRepoRequest"
	cloneRequestType    packageType = "CloneRequest"
	openRequestType     packageType = "OpenRequest"
	// responses
	cloneResponseType packageType = "CloneResponse"
	errorResponseType packageType = "ErrorResponse"
	voidResponseType  packageType = "VoidResponse"
	localRepoType     packageType = "LocalRepo"
)

type packageBasis struct {
	Kind packageType `json:"kind"`
	UUID string      `json:"uuid"`
	Data interface{} `json:"data"`
}

func (pb *packageBasis) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Kind packageType            `json:"kind"`
		UUID string                 `json:"uuid"`
		Data map[string]interface{} `json:"data"`
	}

	err := json.Unmarshal(b, &tmp)
	if err != nil {
		return err
	}

	pb.Kind = tmp.Kind
	pb.UUID = tmp.UUID

	switch pb.Kind {
	case helloType:
		data := helloPackage{}
		err = mapstructure.Decode(tmp.Data, &data)
		pb.Data = data
	case findRepoRequestType:
		data := findRepoRequest{}
		err = mapstructure.Decode(tmp.Data, &data)
		pb.Data = data
	case cloneRequestType:
		data := cloneRequest{}
		err = mapstructure.Decode(tmp.Data, &data)
		pb.Data = data
	case openRequestType:
		data := openRequest{}
		err = mapstructure.Decode(tmp.Data, &data)
		pb.Data = data
	default:
		return fmt.Errorf("Can't unmarshal package of type '%s'", pb.Kind)
	}

	return err
}

type helloPackage struct {
	UUID    string `json:"uuid" mapstructure:"uuid"`
	Version int    `json:"version"  mapstructure:"version"`
}

type findRepoRequest struct {
	RepoName string `json:"repoName"`
}

type cloneRequest struct {
	RepoUrl   string  `json:"repoUrl"`
	LocalPath *string `json:"localPath"`
}

type cloneResponse struct {
	Output string `json:"output"`
}

type openRequest struct {
	Repo localRepo `json:"repo"`
}

type errorResponse struct {
	Msg string `json:"msg"`
}

//

type localRepo struct {
	RemoteRepositiory string  `json:"remoteRepositiory"`
	Path              *string `json:"path"`
}
