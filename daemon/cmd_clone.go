package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os/exec"
	"path/filepath"
)

func cmdClone(ctx *connectionContext, pack packageBasis) {
	request := pack.Data.(cloneRequest)

	var cloneLocation string
	if request.LocalPath != nil && *request.LocalPath != "" {
		cloneLocation = *request.LocalPath
	} else {
		cloneLocation = filepath.Join(homeDir, "git", ctx.local.RemoteRepositiory)
	}

	cmd := exec.Command("git", "clone", request.RepoUrl, cloneLocation)
	stdout, err := cmd.StdoutPipe()
	stderr, err := cmd.StderrPipe()

	done := make(chan bool, 2)
	forward := func(stream io.ReadCloser) {
		scanner := bufio.NewScanner(stream)
		for scanner.Scan() {
			fmt.Printf("pipeing to %s: %s\n", pack.UUID, scanner.Text())
			sendResponse(ctx.conn, cloneResponseType, &cloneResponse{Output: scanner.Text()}, pack)
		}
		if err := scanner.Err(); err != nil {
			//fmt.Fprintln(os.Stderr, "reading standard input:", err)
			fmt.Printf("error to %s: %s\n", pack.UUID, err.Error())
			sendError(ctx.conn, err.Error(), pack)
		}

		fmt.Printf("piping done for %s\n", pack.UUID)
		done <- true
	}
	go forward(stdout)
	go forward(stderr)

	if err = cmd.Run(); err != nil {
		log.Println(err)
		sendError(ctx.conn, "could not clone", pack)
	} else {
		ctx.local.Path = &cloneLocation
	}

	<-done
	<-done
	sendEndStreamResponse(ctx.conn, voidResponseType, nil, pack)
}
