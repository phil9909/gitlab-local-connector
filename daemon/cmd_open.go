package main

func cmdOpen(ctx *connectionContext, pack packageBasis) {
	request := pack.Data.(openRequest)
	osOpen(*request.Repo.Path)
	sendVoid(ctx.conn, pack)
}
