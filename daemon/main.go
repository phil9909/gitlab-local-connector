package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/user"
	"path/filepath"

	"github.com/gorilla/websocket"
	"github.com/speedata/gogit"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(_ *http.Request) bool {
		return true
	},
}

func lookupBranches(repo *gogit.Repository) []string {
	files, err := ioutil.ReadDir(filepath.Join(repo.Path, ".git", "refs", "heads"))
	if err != nil {
		log.Fatal(err)
	}
	branches := make([]string, len(files))
	for i, f := range files {
		branches[i] = f.Name()
	}
	return branches
}

func readStringMessage(conn *websocket.Conn) (string, error) {
	for {
		messageType, msg, err := conn.ReadMessage()
		if err != nil {
			return "", err
		}

		if messageType == websocket.BinaryMessage {
			err := conn.WriteMessage(websocket.TextMessage, []byte("Only Text Messages are supported"))
			if err != nil {
				return "", err
			}
		} else {
			return string(msg), nil
		}
	}
}

func sendResponse(conn *websocket.Conn, kind packageType, data interface{}, to packageBasis) error {
	msgobj := packageBasis{
		Kind: kind,
		UUID: to.UUID,
		Data: data}
	msgobjstr, _ := json.Marshal(msgobj)
	return conn.WriteMessage(websocket.TextMessage, msgobjstr)
}

func sendEndStreamResponse(conn *websocket.Conn, kind packageType, data interface{}, to packageBasis) error {
	return sendResponse(conn, "StreamEnd<"+kind+">", data, to)
}

func sendError(conn *websocket.Conn, msg string, to packageBasis) error {
	return sendResponse(conn, errorResponseType, &errorResponse{Msg: msg}, to)
}

func sendVoid(conn *websocket.Conn, to packageBasis) error {
	return sendResponse(conn, voidResponseType, nil, to)
}

func handshake(conn *websocket.Conn) error {
	msg, err := readStringMessage(conn)
	if err != nil {
		return err
	}

	var pack packageBasis
	err = json.Unmarshal([]byte(msg), &pack)
	if err != nil {
		return err
	}

	if pack.Kind != helloType {
		sendError(conn, "Handshake missing", pack)
		return errors.New("Handshake missing")
	}

	helloPack := pack.Data.(helloPackage)

	if helloPack.Version != 1 {
		sendError(conn, "Unsupported Protocol Version", pack)
		return errors.New("Unsupported Protocol Version")
	}

	// TODO: check uuid

	return sendVoid(conn, pack)

}

type connectionContext struct {
	conn  *websocket.Conn
	local localRepo
}

func handler(w http.ResponseWriter, r *http.Request) {
	var ctx connectionContext
	var err error

	w.Header()["Access-Control-Allow-Origin"] = []string{"chrome-extension://hlepfoohegkhhmjieoechaddaejaokhf"}
	ctx.conn, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	err = handshake(ctx.conn)
	if err != nil {
		log.Println(err)
		return
	}

	for {
		msg, err := readStringMessage(ctx.conn)
		if err != nil {
			log.Println(err)
			return
		}

		var pack packageBasis
		err = json.Unmarshal([]byte(msg), &pack)
		if err != nil {
			log.Println(err)
			return
		}

		if pack.Kind == findRepoRequestType {
			cmdFindRepo(&ctx, pack)
		} else if pack.Kind == cloneRequestType {
			cmdClone(&ctx, pack)
		} else if pack.Kind == openRequestType {
			cmdOpen(&ctx, pack)
		} else {
			sendError(ctx.conn, "unknown command", pack)
		}
	}
}

var homeDir string

func main() {
	fmt.Println("Starting")

	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	homeDir = usr.HomeDir

	http.HandleFunc("/api/", handler)
	fmt.Println(http.ListenAndServe("localhost:7365", nil))
}
